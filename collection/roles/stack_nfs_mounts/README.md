# stack_nfs_mount

This role can be used to mount a nfs share to the host.
It ensures that the directory exists and the permissions are set correctly.
