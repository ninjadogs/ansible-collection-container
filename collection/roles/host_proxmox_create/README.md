# ✨ host_cloud_init_create

This Ansible role provides a convenient way to create and update a virtual machine in Proxmox and tries to be idempotent.
The role variables are prefixed with `host_proxmox_` to make them compatible with the role `host_proxmox_delete`.

## Note

To fully activate the kvm firewall you also have to

* activate firewall on cluster level
* activate firewall on cluster pve node level
* configure firewall on kvm level

See also:

* <https://pve.proxmox.com/wiki/Firewall>
* <https://docs.ansible.com/ansible/latest/collections/community/general/proxmox_kvm_module.html#parameter-net>
