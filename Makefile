SHELL := /bin/bash
.DEFAULT_GOAL := help

# includes
-include makefiles/*

# Path variables
GIT_ROOT := $(shell git rev-parse --show-toplevel)

# Text colors
red = $(shell tput setaf 1)
yellow = $(shell tput setaf 3)
magenta = $(shell tput setaf 5)
reset = $(shell tput sgr0)

# -----------------------------------------------------------------------------
# TARGETS
# -----------------------------------------------------------------------------

.PHONY: help
help: ## Displays all available targets with their descriptions
	@awk 'BEGIN { FS = ":.*##" } \
        /^[a-zA-Z_-]+:.*?##/ { \
          printf "$(yellow)%-25s$(reset) $(magenta)%s$(reset)\n", $$1, $$2 \
        }' $(MAKEFILE_LIST) | sort

.PHONY: bootstrap-fedora
bootstrap-fedora: ## Bootstrap the virtual environment on a fedora system
	@echo -e "\n$(yellow)Install pipenv:$(reset)"
	sudo dnf install -y pipenv

	@echo -e "\n$(yellow)Install virtualenv:$(reset)"
	pipenv install --ignore-pipfile --dev

	@echo -e "\n$(yellow)Install pre-commit-hooks:$(reset)"
	make hooks-update

.PHONY: hooks-update
hooks-update: ## Installs and updates git hooks
	@echo -e "\n$(yellow)Install pre-commit hooks:$(reset)"
	pipenv run pre-commit install
	pipenv run pre-commit install --hook-type post-commit

	@echo -e "\n$(yellow)Autoupdate pre-commit hooks:$(reset)"
	pipenv run pre-commit autoupdate
